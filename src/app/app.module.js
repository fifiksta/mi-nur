angular.module('app',[])
	.directive("focus", function() {
		return {
			link: function($scope, iElm, iAttrs, controller) {
				iElm.focus();
			}
		};
	})
	.filter("format", function(){
		return function(number){
			if (typeof(number) !== 'number') return '';
			return number.toFixed(2);
		};
	})

	.controller('AppCtrl', function($scope, User, Items){
		$scope.itemAddingHidden = false;
		$scope.expandedIndex = -1;
		$scope.items = Items;
		$scope.users = [];
		$scope.showMenu = true;
		$scope.newItemPayerIndex = -1;
		$scope.newItem = {
			payerIndex : 0
		};

		$scope.confirm = function(string){
			return confirm(string);
		};

		$scope.data = {
			view : "all",
			viewUser : undefined
		};

		$scope.selectedPayer = function(){
			if ($scope.newItem.payerIndex === -1)
				return {
					img_src : function(){ "/avatar.png"; },
					color   : function(){ "#888"; }
				};
			return $scope.users[$scope.newItem.payerIndex];
		};

		$scope.toggleExpand = function(index){
			$scope.expandedIndex = ($scope.expandedIndex === index) ? -1 : index;
		};

		$scope.autoEstimateAmount = function(item, auto, user){
			console.log(auto);
			item.setShare(user, (auto? null : item.getEstimatedShare(user)));
		};

		$scope.toggleMenu = function(state) {
			$scope.showMenu = state;
		};

		var email_test = /[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}/;

		$scope.invalidUser = function(email){
			for (var i in $scope.users){
				var user = $scope.users[i];
				if (user.email() === email)
					return "user-exists";
				if (!(email_test.test(email)))
					return "invalid-email";
			}
			return false;
		};

		$scope.addUser = function(email){
			if (!$scope.fetchingNewUser && !$scope.invalidUser(email)){
				$scope.fetchingNewUser = true;
				var user = User.getbyEmail(email);
				user.fetchFromGravatar()
					.success(function(){
						$scope.fetchingNewUser = false;
						$("#addusermodal").modal('hide');
						$scope.modalVisible = false;
						$scope.newusermail = "";
						$scope.showMenu = false;
					})
					.error(function(){
						$scope.fetchingNewUser = false;
						$("#addusermodal").modal('hide');
						$scope.modalVisible = false;
						$scope.newusermail = "";
						$scope.showMenu = false;
					});
				$scope.users.push(user);
			}
		};

		$scope.assignItem = function(item, user, amount){
			if (amount === undefined) amount = null;
			item.setShare(user, amount);
			$scope.data.assignItem = undefined;
			$scope.data.assignAmount = undefined;
		};

		$scope.log = function(what){
			console.log("log:", what);
		};

		$scope.addUserDialog = function(){
			$scope.modalVisible = true;
			$("#addusermodal").modal();
			$("#newEmail").focus();
		};
/*
		$scope.addUser('standa.fifik@gmail.com');
		$scope.fetchingNewUser = false;
		$scope.addUser('omusil@gmail.com');
		$scope.items.addItem('Mléko', 3, 20, $scope.users[0]);
		$scope.items.addItem('Chleba', 1, 20, $scope.users[0]);*/

		/* keeping panel the right height */
		var onWindowResize = function (){
			$scope.desktopWidth = ($(window).width() >= 768);
			$scope.panelHeight = Math.max(0, $(window).height() - ($(".container").position().top + $(".content .panel-footer").height() + $(".content .panel-heading").height() + 50));
			$scope.menuHeight = Math.max(0, $(window).height() - ($(".container").position().top + 50));
		};

		$scope.$watch(function(){
			onWindowResize();
		});

		$(window).resize(function(){
			onWindowResize();
			$scope.$apply();
		});
	});