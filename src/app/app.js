angular.module('app')
	.service('User', function($http){

		var users = {};

		var User = function(email){
			this.init(email.trim());
		};

		User.getbyEmail = function(email){
			if (users[email] === undefined)
				users[email] = new User(email);
			return users[email];
		};

		User.exists = function(email){
			for(var key in users)
				if (key == email)
					return true;
			return false;
		};

		User.prototype = {

			init : function(email){
				this._data = {
					name     : email,
					email    : email,
					img_src  : "avatar.png",
					color    : Math.random()*360
				};
			},

			fetchFromGravatar : function(){
				var user = this;
				return $http.get('avatar.php?email='+this._data.email)
					.success(function(data){
						data = data.entry[0];
						user._data.name = data.displayName;
						user._data.img_src = data.thumbnailUrl;
					});
			},

			name: function(){
				return this._data.name;
			},

			email: function(){
				return this._data.email;
			},

			img_src: function(){
				return this._data.img_src;
			},

			color: function(){
				return HSVtoRGB(this._data.color, 0.636, 0.851);
			}

		};

		return User;
	})
	.service('Items', function(User){

		Item = function(name, amount, price, payer){
			this.init(name, amount, price, payer);
		};

		Item.prototype = {

			init : function(name, amount, price, payer){
				this.name = name;
				this.amount = amount;
				this.price = price;
				this.payer = payer;
				this.share = {};
			},

			totalPrice : function(){
				return this.price * this.amount;
			},

			toggleShare: function(user){
				if (this.hasShare(user))
					this.unsetShare(user);
				else
					this.setShare(user);
			},

			setShare : function(user, share){
				if (share === undefined)
					share = null;
				this.share[user.email()] = share;
			},

			hasShare : function(user){
				return this.share[user.email()] !== undefined;
			},

			unsetShare : function(user){
				delete(this.share[user.email()]);
			},

			getShare : function(user){
				return this.share[user.email()];
			},

			getPriceForUser : function(user){
				var share = this.share[user.email()];
				if (share === undefined)
					return 0;
				if (share === null){
					var shareres = 0;
					var amount = this.amount;
					for (var email in this.share){
						s = this.share[email];
						if (s === undefined)
							continue;
						if (s === null)
							shareres += 1;
						else
							amount -= s;
					}
					if (shareres === 0) return 0; // this should not happen
					return amount * this.price / shareres;
				}
				return share * this.price;
			},

			getEstimatedShare : function(user){
				return this.getPriceForUser(user) / this.price;
			},

			getFreeDividedShare : function(){
				var shareres = 1;
				var amount = this.amount;
				for (var email in this.share){
					s = this.share[email];
					if (s === undefined)
						continue;
					if (s === null)
						shareres += 1;
					else
						amount -= s;
				}
				return amount/shareres;
			},

			getMaxFreeShare : function(){
				var share = 0;
				for (var email in this.share){
					s = this.share[email];
					if (s === undefined || s === null)
						continue;
					share += s;
				}
				return this.amount - share;
			},

			getDistributedPrice : function(){
				var distributed = 0;
				for (var user in this.share){
					distributed += this.getPriceForUser(User.getbyEmail(user));
				}
				return distributed;
			},

			getUndistributedPrice : function(){
				return this.totalPrice() - this.getDistributedPrice();
			},

			getSharers : function(){
				var users = [];
				for (var email in this.share)
					users.push(User.getbyEmail(email));
				return users;
			}
		};

		var Items = function(){

		};


		ItemsProto = function(){
			this.addItem = function(name, amount, price, payer){
				this.push(new Item(name, amount, price, payer));
			};

			this.totalSum = function(){
				return this.reduce(function(sum, item){
					return sum + item.totalPrice();
				}, 0);
			};

			this.sumForUser = function(user){
				if (user === undefined) return;
				return this.reduce(function(sum, item){
					return sum + item.getPriceForUser(user);
				}, 0);
			};

			this.paiedByUser = function(user){
				if (user === undefined) return [];
				return this.filter(function(item){
					return item.payer === user;
				});
			};

			this.totalPaiedByUser = function(user){
				if (user === undefined) return 0;
				return this.paiedByUser(user).reduce(function(sum, item){
					return sum + item.totalPrice();
				},0);
			};

			this.getUndistributedPrice = function(){
				return this.reduce(function(sum, item){
					return sum + item.getUndistributedPrice();
				}, 0);
			};

			this.getUndistributedItems = function(){
				return this.reduce(function(sum, item){
					return item.getDistributedPrice() === 0? sum + 1 : sum;
				}, 0);
			};

			this.assignedToUser = function(user){
				if (user === undefined) return;
				return this.filter(function(item){
					return item.hasShare(user);
				});
			};
			this.notAssignedToUser = function(user){
				if (user === undefined) return;
				return this.filter(function(item){
					return !item.hasShare(user);
				});
			};
		};


		ItemsProto.prototype = Array();
		Items.prototype = new ItemsProto();
		return new Items();
	});







function HSVtoRGB(h, s, v) {
	var r, g, b, i, f, p, q, t;
	if (h && s === undefined && v === undefined) {
		s = h.s, v = h.v, h = h.h;
	}
	i = Math.floor(h * 6);
	f = h * 6 - i;
	p = v * (1 - s);
	q = v * (1 - f * s);
	t = v * (1 - (1 - f) * s);
	switch (i % 6) {
		case 0: r = v, g = t, b = p; break;
		case 1: r = q, g = v, b = p; break;
		case 2: r = p, g = v, b = t; break;
		case 3: r = p, g = q, b = v; break;
		case 4: r = t, g = p, b = v; break;
		case 5: r = v, g = p, b = q; break;
	}
	toHex = function(number) {
		number = Math.round(number*255).toString(16);
		if (number.length < 1)
			number = "0"+number;
		return number;
	};
	return "#"+[r,g,b].map(toHex).join("");
}