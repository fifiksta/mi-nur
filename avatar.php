<?php
$email = $_GET['email'];
$grav_url = "http://gravatar.com/" . md5( strtolower( trim( $email ) ) ) . ".php";

$data = file_get_contents($grav_url);
$data = json_encode(unserialize($data));

if ($data != "false"){
	header('Content-Type: application/json', true, 200);
	echo $data;
} else {
	header('Content-Type: application/json', true, 404);
}
