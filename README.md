# Rozpočítání nákupu mezi přátele
Aplikace má uživateli umožnit snadné rozpočítání ceny nákupu mezi několik osob, vhodná je především na společné akce s nákupem před akcí typu „víkend na chatě“. Poté co, uživatel přepíše účtenku bude moci snadno rozdělit položky z účtenky mezi zadané uživatele, za to dostane přehledné informace pro všechny účastníky akce o tom na kolik je akce vyšla a jakým způsobem se s ostatními finančně vyrovnat. Tyto informace může snadno sdílet s ostatními buď pomocí emailu odeslaného přímo z aplikace, nebo ručně sdílením vygenerovaného odkazu.

[Rozvaha na eduxu](https://edux.fit.cvut.cz/courses/MI-NUR/student/fifiksta/nur1)

[Low-fine prototype na eduxu](https://edux.fit.cvut.cz/courses/MI-NUR/_media/student/fifiksta/lofi.pdf)

[High-fine prototype demo](http://fisoft.eu/ficik/nur/)
